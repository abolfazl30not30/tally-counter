package sbu.cs.excercises;

import sbu.cs.IllegalValueException;

import java.util.Random;

public class ExerciseLecture5 {

    /**
     *   implement a function to create a random password with
     *   given length using lower case letters
     *   lecture 5 page 14
     * @param length length of the wanted password
     */
    public String weakPassword(int length) {
        String answer = "";
        Random r = new Random();
        for(int i = 0; i < length; i++){
            char c = (char)(r.nextInt(26) + 'a');
            answer += c;
        }
        return answer;
    }

    /**
     *   implement a function to create a random password with
     *   given length and at least 1 digit and 1 special character
     *   lecture 5 page 14
     * @param length length of the wanted password
     */
    public String strongPassword(int length){
        if(length < 3){
            throw new IllegalValueException();
        }
        else {
            Random r = new Random();
            String answer = "";
            String lowerCaseLetters = "abcdefghijklmnopqrstuvwxyz";
            String specialCharacters = "!@#$";
            String numbers = "1234567890";
            String combinedChars = lowerCaseLetters + specialCharacters + numbers;


            answer += specialCharacters.charAt(r.nextInt(specialCharacters.length()));
            answer += numbers.charAt(r.nextInt(numbers.length()));
            answer += lowerCaseLetters.charAt(r.nextInt(lowerCaseLetters.length()));

            for(int i = 3; i < length; i++){
                answer += combinedChars.charAt(r.nextInt(combinedChars.length()));
            }
            return answer;
        }

    }

    /**
     *   implement a function that checks if an integer is a fibobin number
     *   integer n is fibobin if there exist an i where:
     *       n = fib(i) + bin(fib(i))
     *   where fib(i) is the ith fibonacci number and bin(i) is the number
     *   of ones in binary format
     *   lecture 5 page 17
     * @param n the number n
     */
    public boolean isFiboBin(int n) {
        return false;
    }
}
