package sbu.cs.excercises;

import sbu.cs.IllegalValueException;

public class ExerciseLecture4 {

    /**
     *   implement a function that returns factorial of given n
     *   lecture 4 page 15
     * @exception IllegalValueException when the factorial of n doesn't exist
     * @param n an integer which the function should return its factorial, namely
     *          n!
     */
    public long factorial(int n) throws IllegalValueException {
        long ans = 1;

        // check that n is negative
        if(n < 0){
            throw new IllegalValueException();
        }

        //To calculate factorial
        else {
            for (int i = n; i > 1; i--) {
                ans *= i;
            }
        }
        return ans;
    }

    /**
     *   implement a function that return nth number of fibonacci series
     *   the series -> 1, 1, 2, 3, 5, 8, ...
     *   lecture 4 page 19
     * @param n an integer that indicated the nth number in fibonacci series
     */
    public long fibonacci(int n) {

        if (n <= 1) {
            return n;
        }

        long previousN = 0;
        long currentN = 1;

        for (int i = 0; i < n - 1; i++)
        {
            long newN = previousN + currentN;
            previousN = currentN;
            currentN = newN;
        }
        return currentN;
    }

    /**
     *   implement a function that return reverse of a given word
     *   lecture 4 page 19
     * @param word the word as a string
     */

    public String reverse(String word) {

        String answer = "";
        for(int n = word.length() - 1; n >=0; n--){
            answer += word.charAt(n);
        }
        return answer;
    }

    /**
     *   implement a function that returns true if the given line is
     *   palindrome and false if it is not palindrome.
     *   palindrome is like 'wow', 'never odd or even', 'Wow'
     *   lecture 4 page 19
     *
     * @param line the line of text as a string
     */
    public boolean isPalindrome(String line) {
        //removing space
        line = line.replaceAll(" ","");

        //covert uppercase word to lowercase
        line = line.toLowerCase();

        String reverse = reverse(line);
        if(line.equals(reverse)){
            return true;
        }
        else{
            return false;
        }

    }

    /**
     *   implement a function which computes the dot plot of 2 given
     *   string. dot plot of hello and hello is:
     *       h e l l o
     *   h   *
     *   e     *
     *   l       * *
     *   l       * *
     *   o           *
     *   lecture 4 page 26
     * @param string1 the first string
     * @param string2 the second string
     */
    public char[][] dotPlot(String string1, String string2) {

        int row = string1.length();
        int col = string2.length();

        char[][] answer = new char[row][col];

        for(int i = 0; i < string1.length(); i++){
            for(int j = 0; j < string2.length(); j++){
                if(string1.charAt(i) == string2.charAt(j)){
                    answer[i][j] = '*';
                }
                else {
                    answer[i][j] = ' ';
                }
            }
        }

        return answer;
    }
}
