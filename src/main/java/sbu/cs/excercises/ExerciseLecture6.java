package sbu.cs.excercises;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class ExerciseLecture6 {

    /**
     *   implement a function that takes an array of int and return sum of
     *   elements at even positions
     *   lecture 6 page  16
     * @param array an integer array
     */
    public long calculateEvenSum(int[] array) {
        long sum = 0;
        for(int i = 0; i < array.length; i += 2){
            sum += array[i];
        }
        return sum;
    }

    /**
     *   implement a function that takes an array of int and return that
     *   array in reverse order
     *   lecture 6 page 16
     * @param array an integer array
     */

    public int[] reverseArray(int[] array) {
        int end = array.length - 1;
        for(int i = 0; i < array.length/2; i++){
            int temp = array[i];
            array[i] = array[end - i];
            array[end - i] = temp;
        }
        return array;
    }

    /**
     *   implement a function that calculate product of two 2-dim matrices
     *   lecture 6 page 21
     * @param matrix1 the first matrix
     * @param matrix2 the second matrix
     */
    public double[][] matrixProduct(double[][] matrix1, double[][] matrix2) throws RuntimeException {
        //get column and row of two matrix
        int row1 = matrix1.length;
        int col1 = matrix1[0].length;
        int col2 = matrix2[0].length;

        //declare new array for matrix product
        double[][] answer = new double[row1][col2];
        //initialized
        for(int i =0; i < row1; i++){
            for(int j = 0; j  < col2; j++){
                answer[i][j] = 0;
            }
        }

        for(int i =0; i < row1; i++){
            for(int j = 0; j < col2; j++){
                for(int k = 0; k < col1; k++){
                    answer[i][j] += (matrix1[i][k] * matrix2[k][j]);
                }
            }
        }

        return answer;
    }

    /**
     *   implement a function that return array list of array list of string
     *   from a 2-dim string array
     *   lecture 6 page 30
     * @param names a two-dimensional array of strings
     */
    public List<List<String>> arrayToList(String[][] names) {

        int iLength = names.length;

        List<List<String>> answer = new ArrayList<>(iLength);

        for (int i = 0; i < iLength; ++i) {
            int jLength = names[0].length;
            answer.add(new ArrayList(jLength));
            for (int j = 0; j < jLength; ++j) {
                answer.get(i).add(names[i][j]);
            }
        }

        return answer;
    }

    /**
     *   implement a function that return a list of prime factor of integer n
     *   in ascending order
     *   lecture 6 page 30
     * @param n the number n
     */
    public List<Integer> primeFactors(int n) {

        List<Integer> prime = new ArrayList<>();
        for(int i = 2; i < n; i++) {
            while(n%i == 0) {
                prime.add(i);
                n = n / i;
            }
        }
        if(n >2){
            prime.add(n);
        }

        //Remove Duplicates from ArrayList
        prime = prime.stream().distinct().collect(Collectors.toList());

        return prime;
    }

    /**
     *   implement a function that return a list of words in a given string.
     *   consider that words are separated by spaces, commas,questions marks....
     *   lecture 6 page 30
     */
    public List<String> extractWord(String line) {

        //split the words
        String[] words = line.split("[[ ]*|[,]*|[\\.]*|[:]*|[/]*|[!]*|[?]*|[+]*]+");

        //convert the Array to the Array list
        List<String> wordsList = List.of(words);

        return wordsList;
    }
}
