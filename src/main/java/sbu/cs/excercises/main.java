package sbu.cs.excercises;

public class main {
    public static void main(String[] args) {
        int [] arr = {1,2,3,4};
        ExerciseLecture6 ex = new ExerciseLecture6();
        int [] ans = new int[4];
        ans = ex.reverseArray(arr);

        for(int i = 0; i < ans.length;i++){
            System.out.print(ans[i]);
            System.out.print(" ");
        }
    }
}
