package sbu.cs.excercises;

import sbu.cs.IllegalValueException;
import sbu.cs.TallyCounterInterface;

import java.util.concurrent.ExecutionException;

/**
 * Implement the following functions and add fields
 * to the class below so that it resembles the functionality
 * of a real tally counter.
 * Consider a tally counter which can count up to 9999.
 */
public class TallyCounter implements TallyCounterInterface {

    /**
     * Increments the counter of tally counter by one
     */

    private int value;

    public TallyCounter(){
        this.value = 0;
    }

    @Override
    public void count() {
        if(this.value < 9999){
            this.value++;
        }
        else {
            return;
        }
    }

    /**
     * @return the current value of counter
     */
    @Override
    public int getValue() {
        return this.value;
    }

    /**
     * Sets a new value for counter
     * @param newCounterValue the new value
     * @throws IllegalValueException whenever the input value is not valid.
     * think of the scenarios in which the value is unacceptable
     */
    @Override
    public void setValue(int newCounterValue) throws IllegalValueException {
        if(newCounterValue < 0 || newCounterValue > 9999){
            throw new IllegalValueException();
        }
        else {
            this.value = newCounterValue;
        }
    }

    /**
     * resets the counter
     */
    @Override
    public void reset() {
        this.value = 0;
    }
}
